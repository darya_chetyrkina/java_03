public class EndStringFilter implements IFilter{
    String pattern;

    public EndStringFilter(String s){
        this.pattern = s;
    }

    public boolean apply(String str){
        return str.endsWith(pattern);
    }
}
