import java.util.Objects;

public class PackedWeightProduct extends WeightProduct implements PackedProduct{
    double weight;
    private PackageOfProduct pack;

    public PackedWeightProduct(WeightProduct product, double weight, PackageOfProduct pack){
        super(product.getProductName(), product.getProductDescription());
        setWeight(weight);
        setPack(pack);
    }

    public double getNettMass(){
        return weight;
    }

    @Override
    public double getGrossMass(){
        return weight+pack.getPackageWeight();
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight < 0) throw new IllegalArgumentException("Mass cannot be negative!");
        this.weight = weight;
    }

    @Override
    public void setPack(PackageOfProduct pack) {
        this.pack = pack;
    }

    @Override
    public PackageOfProduct getPack() {
        return pack;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackedWeightProduct)) return false;
        if (!super.equals(o)) return false;
        PackedWeightProduct that = (PackedWeightProduct) o;
        return Double.compare(that.getWeight(), getWeight()) == 0 && Objects.equals(getPack(), that.getPack());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPack(), getWeight());
    }

    @Override
    public String toString() {
        return String.format("Товар: %s%n Масса: %fкг%n Описание: %s%n Упаковано: $s", getProductName(), getWeight(), getProductDescription(), pack.getPackageName());
    }
}