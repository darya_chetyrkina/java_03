import java.util.Arrays;
import java.util.Objects;

public class ProductBatch{

    private String batchDescription;
    private PackedProduct[] batch;

    public ProductBatch(String batchDescription, PackedProduct ... batch){
        if (batch == null) throw new IllegalArgumentException("Batch is empty!");
        setBatchDescription(batchDescription);
        this.batch = batch;
    }

    public String getBatchDescription() {
        return batchDescription;
    }

    public void setBatchDescription(String batchDescription) {
        this.batchDescription = batchDescription;
    }

    public PackedProduct[] getBatch() {
        return batch;
    }

    public void setBatch(PackedProduct[] batch) {
        this.batch = batch;
    }

    public double getBatchGrossMass(){
        double mass = 0;
        for (PackedProduct p: batch) {
            mass+=p.getGrossMass();
        }
        return mass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductBatch)) return false;
        ProductBatch that = (ProductBatch) o;
        return Objects.equals(getBatchDescription(), that.getBatchDescription()) && Arrays.equals(batch, that.batch);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getBatchDescription());
        result = 31 * result + Arrays.hashCode(batch);
        return result;
    }

    @Override
    public String toString() {
        return String.format("Партия товаров: %s", batchDescription);
    }
}
