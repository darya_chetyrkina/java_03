public class ProductService {
    public static int countByFilter(ProductBatch batch, IFilter filter){
        int count = 0;
        for (PackedProduct p: batch.getBatch()) {
            if (filter.apply(p.getProductName())){
                count++;
            }
        }
        return count;
    }
}
