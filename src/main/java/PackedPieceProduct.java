import java.util.Objects;

public class PackedPieceProduct extends PieceProduct implements PackedProduct{
    private int count;
    PackageOfProduct pack;

    public PackedPieceProduct(PieceProduct product, int count, PackageOfProduct pack){
        super(product.getProductName(),product.getProductDescription(),product.getWeight());
        setCount(count);
        setPack(pack);
    }

    public double getNettMass(){
        return count*getWeight();
    }

    @Override
    public double getGrossMass(){
        return count*getWeight()+ pack.getPackageWeight();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public PackageOfProduct getPack() {
        return pack;
    }

    public void setPack(PackageOfProduct pack) {
        if (pack == null) throw new IllegalArgumentException("Pack is null!");
        this.pack = pack;
    }

    public String toString(){
        return String.format("Товар: %s%n Масса: %fкг%n Описание: %sn Количество: %f шт %n Упаковано: $s", getProductName(), getWeight(), getProductDescription(), count, pack.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackedPieceProduct)) return false;
        if (!super.equals(o)) return false;
        PackedPieceProduct that = (PackedPieceProduct) o;
        return getCount() == that.getCount() && Objects.equals(getPack(), that.getPack());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPack(), getCount());
    }
}
