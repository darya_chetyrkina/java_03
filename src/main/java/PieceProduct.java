import java.util.Objects;

public class PieceProduct extends Product{
    private double weight;

    public PieceProduct(String name, String description, double mass){
        super(name, description);
        setWeight(mass);
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
       if (weight < 0) throw new IllegalArgumentException("Mass cannot be negative");
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PieceProduct)) return false;
        if (!super.equals(o)) return false;
        PieceProduct that = (PieceProduct) o;
        return Double.compare(that.getWeight(), getWeight()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getWeight());
    }

    @Override
    public String toString() {
        return String.format("Товар %sn Macca: %fкг: Описание:%s", getProductName(), weight, getProductDescription());
    }
}
