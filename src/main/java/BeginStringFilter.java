public class BeginStringFilter implements IFilter{
    String pattern;

    public BeginStringFilter(String s){
        this.pattern = s;
    }

    public boolean apply(String str){
        return str.startsWith(pattern);
    }
}
