import java.util.Objects;
public class Product {
    private String name;
    private String description;

    public Product(String name, String description){
        setProductName(name);
        setProductDescription(description);
    }

    public String getProductName() {
        return name;
    }

    public void setProductName(String name) {
        if (name.isEmpty()) throw new IllegalArgumentException("Name is null!");
        this.name = name;
    }

    public String getProductDescription() {
        return description;
    }

    public void setProductDescription(String description) {
        if (name.isEmpty()) throw new IllegalArgumentException("Description is null!");
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Objects.equals(getProductName(), product.getProductName()) && Objects.equals(getProductDescription(), product.getProductDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductName(), getProductDescription());
    }

    @Override
    public String toString() {
        return String.format("Товар %s: %s", name, description);
    }
}
