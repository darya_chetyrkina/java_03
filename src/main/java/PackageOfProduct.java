import java.util.Objects;

public class PackageOfProduct{
    private String name;
    private double weight;
    public PackageOfProduct(String name, double weight){
        setPackageName(name);
        setPackageWeight(weight);
    }

    public void setPackageName(String name) {
        if (name.isEmpty()) throw new IllegalArgumentException("Name is null!");
        this.name = name;
    }

    public String getPackageName() {
        return name;
    }

    public void setPackageWeight(double weight) {
        if (weight < 0) throw new IllegalArgumentException("Weight cannot be negative!");
        this.weight = weight;
    }

    public double getPackageWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackageOfProduct)) return false;
        PackageOfProduct that = (PackageOfProduct) o;
        return Double.compare(that.getPackageWeight(), getPackageWeight()) == 0 && Objects.equals(getPackageName(), that.getPackageName());
    }

    @Override
    public String toString() {
        return String.format("Упаковка %s: %fкг", name, weight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPackageName(), getPackageWeight());
    }
}
