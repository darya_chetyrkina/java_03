public class ContainsStringFilter implements IFilter{
    String pattern;

    public ContainsStringFilter(String s){
        this.pattern = s;
    }

    public boolean apply(String str){
        int index = str.indexOf(pattern);
        if (index < 0) return false;
        return true;
    }
}
