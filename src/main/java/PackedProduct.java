public interface PackedProduct{

    PackageOfProduct getPack();
    void setPack(PackageOfProduct pack);
    double getNettMass();
    double getGrossMass();
    String getProductName();
}
