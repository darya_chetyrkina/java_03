import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PackedPieceProductTest {

    @Test
    void getNettMass() {
        PieceProduct product = new PieceProduct(" "," ",1);
        PackageOfProduct pack = new PackageOfProduct(" ",0);
        PackedPieceProduct packedProduct = new PackedPieceProduct(product, 5, pack);
        assertEquals(5, packedProduct.getNettMass());
    }

    @Test
    void getNettMass2() {
        PieceProduct product = new PieceProduct(" "," ",1);
        PackageOfProduct pack = new PackageOfProduct(" ",0);
        PackedPieceProduct packedProduct = new PackedPieceProduct(product, 0, pack);
        assertEquals(0, packedProduct.getNettMass());
    }
    @Test
    void getGrossMass() {
        PieceProduct product = new PieceProduct(" "," ",1);
        PackageOfProduct pack = new PackageOfProduct(" ",1);
        PackedPieceProduct packedProduct = new PackedPieceProduct(product, 5, pack);
        assertEquals(6, packedProduct.getGrossMass());
    }

    @Test
    void getGrossMass2() {
        PieceProduct product = new PieceProduct(" "," ",0);
        PackageOfProduct pack = new PackageOfProduct(" ",5);
        PackedPieceProduct packedProduct = new PackedPieceProduct(product, 5, pack);
        assertEquals(5, packedProduct.getGrossMass());
    }


}