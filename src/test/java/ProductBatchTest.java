import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductBatchTest {
    @Test
    void getBatchGrossMass(){
        PackedPieceProduct p1 = new PackedPieceProduct(new PieceProduct(" ", " ", 2),2,new PackageOfProduct(" ", 1));
        PackedWeightProduct p2 = new PackedWeightProduct(new WeightProduct(" ", " "),2,new PackageOfProduct(" ", 1));
        ProductBatch batch = new ProductBatch(" ", p1,p2);
        assertEquals(8, batch.getBatchGrossMass());

    }
}