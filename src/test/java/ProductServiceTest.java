import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductServiceTest {

    @Test
    void countByFilter() {
        PackedPieceProduct p1 = new PackedPieceProduct(new PieceProduct("1", " ", 2),2,new PackageOfProduct(" ", 1));
        PackedWeightProduct p2 = new PackedWeightProduct(new WeightProduct("2", " "),2,new PackageOfProduct(" ", 1));
        ProductBatch batch = new ProductBatch(" ", p1,p2);
        BeginStringFilter filter = new BeginStringFilter("1");
        assertEquals(1, ProductService.countByFilter(batch,filter));
    }

    @Test
    void countByFilter2() {
        PackedPieceProduct p1 = new PackedPieceProduct(new PieceProduct("13", " ", 2),2,new PackageOfProduct(" ", 1));
        PackedWeightProduct p2 = new PackedWeightProduct(new WeightProduct("32", " "),2,new PackageOfProduct(" ", 1));
        ProductBatch batch = new ProductBatch(" ", p1,p2);
        EndStringFilter filter = new EndStringFilter("3");
        assertEquals(1, ProductService.countByFilter(batch,filter));
    }

    @Test
    void countByFilter3() {
        PackedPieceProduct p1 = new PackedPieceProduct(new PieceProduct("13", " ", 2),2,new PackageOfProduct(" ", 1));
        PackedWeightProduct p2 = new PackedWeightProduct(new WeightProduct("32", " "),2,new PackageOfProduct(" ", 1));
        ProductBatch batch = new ProductBatch(" ", p1,p2);
        ContainsStringFilter filter = new ContainsStringFilter("3");
        assertEquals(2, ProductService.countByFilter(batch, filter));
    }
}