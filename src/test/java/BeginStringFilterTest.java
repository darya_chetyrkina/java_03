import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BeginStringFilterTest {

    @Test
    void apply() {
        BeginStringFilter str = new BeginStringFilter("Hello");
        assertTrue(str.apply("Hello, world!"));

    }

    @Test
    void apply2() {
        BeginStringFilter str = new BeginStringFilter("1");
        assertFalse(str.apply("Hello, world!"));

    }
}