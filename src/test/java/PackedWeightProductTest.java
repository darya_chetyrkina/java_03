import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PackedWeightProductTest {

    @Test
    void getNettMass() {
        WeightProduct product = new WeightProduct(" ", " ");
        PackageOfProduct pack = new PackageOfProduct(" ",1);
        PackedWeightProduct packedProduct = new PackedWeightProduct(product, 5, pack);
        assertEquals(5, packedProduct.getNettMass());
    }

    @Test
    void getNettMass2() {
        WeightProduct product = new WeightProduct(" ", " ");
        PackageOfProduct pack = new PackageOfProduct(" ",0);
        PackedWeightProduct packedProduct = new PackedWeightProduct(product, 0, pack);
        assertEquals(0, packedProduct.getNettMass());
    }

    @Test
    void getGrossMass() {
        WeightProduct product = new WeightProduct(" "," ");
        PackageOfProduct pack = new PackageOfProduct(" ",1);
        PackedWeightProduct packedProduct = new PackedWeightProduct(product, 5, pack);
        assertEquals(6, packedProduct.getGrossMass());
    }

    @Test
    void getGrossMass2() {
        WeightProduct product = new WeightProduct(" "," ");
        PackageOfProduct pack = new PackageOfProduct(" ",0);
        PackedWeightProduct packedProduct = new PackedWeightProduct(product, 5, pack);
        assertEquals(5, packedProduct.getGrossMass());
    }



}