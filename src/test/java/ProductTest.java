import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ProductTest {

    @Test
    void setNameException() {
        Assertions.assertThrows(Exception.class, () ->
                new Product(null, ""));
    }

    @Test
    void setDescriptionException() {
        Assertions.assertThrows(Exception.class, () ->
                new Product("", null));
    }
}