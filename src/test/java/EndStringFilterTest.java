import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EndStringFilterTest {

    @Test
    void apply() {
        EndStringFilter str = new EndStringFilter("!");
        assertTrue(str.apply("Hello, world!"));

    }

    @Test
    void apply2() {
        EndStringFilter str = new EndStringFilter("1");
        assertFalse(str.apply("Hello, world!"));

    }

}