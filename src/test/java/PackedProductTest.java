import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PackedProductTest {

    @Test
    void setPack() throws Exception{
        Assertions.assertThrows(Exception.class, () -> {
            new PackedPieceProduct(new PieceProduct(" "," ",1),1, new PackageOfProduct("",0));
        } );
    }

    @Test
    void setPack2() throws Exception {
        Assertions.assertThrows(Exception.class, () -> {
            new PackedWeightProduct(new WeightProduct(" ", " "), 1, new PackageOfProduct("", 0));
        });
    }
}