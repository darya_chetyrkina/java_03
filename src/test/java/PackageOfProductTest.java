import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PackageOfProductTest {

    @Test
    void setNameException() {
        Assertions.assertThrows(Exception.class, () ->
            new PackageOfProduct(null, 1));
    }

    @Test
    void setWeightException() {
        Assertions.assertThrows(Exception.class, () ->
            new PackageOfProduct(null, 1));
    }
}