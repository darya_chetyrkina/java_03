import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContainsStringFilterTest {

    @Test
    void apply() {
        ContainsStringFilter str = new ContainsStringFilter(",");
        assertTrue(str.apply("Hello, world!"));

    }

    @Test
    void apply2() {
        ContainsStringFilter str = new ContainsStringFilter("1");
        assertFalse(str.apply("Hello, world!"));

    }

}